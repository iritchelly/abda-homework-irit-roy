module bitbucket.org/iritchelly/abda-homework-irit-roy

go 1.14

require (
	bitbucket.org/dtolpin/infergo v0.8.4
	gonum.org/v1/plot v0.7.0
)
